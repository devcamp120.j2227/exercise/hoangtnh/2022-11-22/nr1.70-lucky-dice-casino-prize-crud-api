//import thư viện express JS
const express = require ("express");

//import mongoose 
const mongoose = require("mongoose");

//Khai báo app 
const app = express ();

//khai báo cổng chạy app
const port = 8000;
//import thư viện path
const path = require ("path");
//cấu hình request đọc được json body
app.use(express.json());
//Khai báo model 
const userModel = require("./app/models/userModel");
const historyDiceModel = require ("./app/models/diceHistoryModel");
const prizeModel = require("./app/models/prizeModel");
const voucherModel = require("./app/models/voucherModel")

//kết nối với mongo db
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_dice",(error) => {
    if(error) throw error;
    console.log("Connect dice casino to MongoDB successfully!");
})

//khai báo router
const randomNumberRouter = require("./app/routes/randomNumberRouter");
const userRouter = require ("./app/routes/userRouter");
const diceHisRouter = require ("./app/routes/diceHistoryRouter");
const prizeRouter = require("./app/routes/prizeRouter");

//app sử dụng middleware mỗi lần chạy
app.use("/api", randomNumberRouter);
app.use("/api", userRouter);
app.use("/api", diceHisRouter);
app.use("/api", prizeRouter);

//call api chạy browser
app.get("/",(request, response)=>{
    response.sendFile(path.join(__dirname + "/views/Task 31.30.html"))
});
//hiển thị hình ảnh cần thêm  middleware static vào express
app.use(express.static(__dirname +"/views"));
//app chạy trên cổng đã khai báo
app.listen(port, ()=>{
    console.log(`App is running on port ${port}`);
})