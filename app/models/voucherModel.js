//import thư viện mongoose
const mongoose = require("mongoose");
//import thư viện schema class
const Schema = mongoose.Schema;

//tạo class voucher schema
const voucherSchema = new Schema ({
    code: {
        type: String, 
        unique: true, 
        required: true
    },
	discount: {
        type: Number, 
        required: true
    },
	note: {
        type: String
    },
    createdAt: {
        type: Date, 
        default: Date.now()
    },
    updatedAt: {
        type: Date, 
        default: Date.now()
    }
});
module.exports = mongoose.model("Voucher", voucherSchema);